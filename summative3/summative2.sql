select siswa.name_std as 'Nama Siswa', matkul.name_lesson as 'Mata Kuliah',matkul.level_lesson as 'Level Matkul', nilai.score as 'Nilai' 
from score nilai 
join student siswa 
on nilai.student_ID = siswa.id_student 
join lesson matkul
on nilai.lesson_ID = matkul.id_lesson
order by siswa.name_std asc;