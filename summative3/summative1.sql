create database summativedb;

use summativedb;

create table student(id_student int not null auto_increment primary key,
					name_std varchar(50) not null,
                    surname varchar(50) not null,
                    birthdate date,
                    gender enum('Pria','Wanita'))engine= InnoDB;

create table lesson(id_lesson int not null auto_increment primary key,
					name_lesson varchar(50) not null,
                    level_lesson enum('Beginner','Intermmediate','Advanced'))engine = InnoDB;

create table score(id_score int not null auto_increment primary key,
					student_ID int not null,
                    lesson_ID int not null,
                    score int not null, 
                    foreign key(student_ID) references student(id_student), 
                    foreign key(lesson_ID) references lesson(id_lesson))engine = InnoDB;

insert into student(name_std, surname, birthdate, gender)
	values 	('Muhammad Anies Wahdie','Anies','1999-12-22','Pria'),
			('Akbar Widyo','Akbar','1997-01-13','Pria'),
			('Melani Dian','Melani','1998-05-17','Wanita'),
            ('Ayu Rosmalina','Ros','1996-05-03','Wanita'),
            ('Bayu Seno','Seno','1997-03-05','Pria');
    
select * from student;

insert into lesson(name_lesson, level_lesson)
	values 	('Java','Beginner'),
			('Java','Intermmediate'),
            ('Java','Advanced'),
            ('MySQL','Beginner'),
            ('MySQL','Intermmediate'),
            ('MySQL','Advanced');

select * from lesson;

insert into score(student_ID, lesson_ID, score)
	values	(1,7,80),
			(2,7,75),
            (3,11,100),
            (4,12,60),
            (5,9,82),
            (1,8,78);

select * from score;