select 	department.department_id as 'ID Department', 
department.department_name as 'Nama Department', 
year(employee.hire_date) as 'Tahun', 
count(department.department_id) as 'Jumlah Karyawan' 
from employee 
join department 
on employee.department_id = department.department_id
group by employee.department_id
order by employee.department_id asc;